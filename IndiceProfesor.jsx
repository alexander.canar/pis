import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { obtenerTodo, ObtenerExternal } from '../hooks/Conexion';
import { ListaMaterias } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useNavigate } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import Modal from 'react-bootstrap/Modal';

const IndiceProfesor = () => {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const [data, setData] = useState([]);
  const navegation = useNavigate();
  const [docente, setDocente] = useState('');

  useEffect(() => {
    obtenerTodo()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSession();
          mensajes(info.mensaje);
          navegation("/sesion");
        } else {
          console.log(info);
          setData(info);
          if (info.length > 0) {
            setDocente(info[0].profesor.nombres);
          }
        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);

  const columns = [
    {
      name: 'Materia',
      selector: (row) => row.materia.nombre
    },
    {
      name: 'Unidades',
      selector: (row) => row.materia.unidades
    },
    {
      name: 'Acciones',
      cell: (row) => (
        <button className="btn btn-primary" onClick={() => handleAgregarTarea(row.external_id)}>
          Asignar Practica
        </button>
      ),
    },
  ];

  const handleAgregarTarea = (external) => {
    navegation(`/TareasCreadas/${external}`);
  };

  return (
    <>
    <center><h1>INDICE PROFESOR {docente}</h1></center>
    
    
      
     <div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Home</a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Perfil</a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Materias</a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Cursos</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list"></div>
      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list"></div>
      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list"></div>
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list"></div>
    </div>
  </div>
</div>
    </>
   
  );
};

export default IndiceProfesor;
