import React from 'react';
import '../css/Indice.css';
import { useNavigate } from 'react-router';
import CursosCompletados from './Cursoscompletados';
import ListaTareasPendientes from './ListaTareasPendientes';
const Indice = () => {
  const navigation = useNavigate();
  const handleAtrasados = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/ListarTareasAtrasadas');
  };
  const handleCursos = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/Cursoscompletados');
  };
  const handlesalir = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/');
  };
  const TareasCreadas = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...
    // Redirige al usuario a la página de inicio
    navigation('/login');
  };
  return (

    <div className='InicioWindow'>
      <div className='NavSuperior' id='NavSuperior'>
        <nav className="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav ml-auto">
            <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
              <div className="input-group">
                <input type="text" className="form-control bg-light border-0 small" placeholder="Buscar" aria-label="Search" aria-describedby="basic-addon2" />
                <button className="btn btn-primary" type="button">Bucar</button>
              </div>
            </form>
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={TareasCreadas}>Perfil</a>
            </li>
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configuraciones</a>
            </li>
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={handlesalir}>Salir</a>
            </li>
            <li className="nav-item dropdown no-arrow">
              <a className="nav-link dropdown-toggle mr-9" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Nombre del Usuario logeado</a>
            </li>
          </ul>
        </nav>
      </div>


      <div className='NavLateral' id='NavLateral'>
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

          <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div className="sidebar-brand-icon rotate-n-15">
              <i className="fas fa-laugh-wink"></i>
            </div>
            <div className="sidebar-brand-text mx-3">PROYECTO INTEGRADOR</div>
          </a>

          <hr className="sidebar-divider my-0" />

          <li className="nav-item active">
            <a className="collapse-item" href="ListaCursos" >Cursos</a>
          </li>

          <hr className="sidebar-divider my-0" /> 
          
          <li className="nav-item active">
            <a className="collapse-item" href="#" >Tareas</a>
            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div className="bg-white py-2 collapse-inner rounded">
                <a className="collapse-item" href="#">Entregadas</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="#" onClick={handleAtrasados}>Atrasados</a>
              </div>
            </div>
          </li>

          <hr className="sidebar-divider" />

          <div className="sidebar-heading"></div>

          <li className="nav-item">
            <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              <i className="fas fa-fw fa-cog"></i>
              <span>Cursos</span>
            </a>
            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div className="bg-white py-2 collapse-inner rounded">
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="buttons.html">En curso</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="#" >Finalizados</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="cards.html">Acreditaciones</a>

              </div>
            </div>
          </li>
          <hr className="sidebar-divider" />
          <div className="sidebar-heading">
            Progreso
          </div>

          <li className="nav-item">
            <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages" />
          </li>
        </ul>
      </div>
<ListaTareasPendientes/>
    </div>

   
  )
};

export default Indice;
