import React from 'react';    
import '../css/listaTareas.css'
const ListarTareasAtrasadas = () => {
    return (
        
        <div className='col-12' id='usuario'>
  <center>
    <b><label className='form-label'>Tareas Pendientes: </label></b>
    <input className="busqueda" placeholder="&#128270; Buscar" />
    <button className='btn btn col-1'>BUSCAR</button>
  </center>

  <br />
  <div className='tabla'>
    <table className="table table-bordered">
      <thead className="thead-dark">
        <tr>
          <th>Nombre</th>
          <th>Ciclo</th>
          <th>Paralelo</th>
          <th>Fecha Envio</th>
          <th>Fecha Entrega</th>
          <th>Estado</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

    );
};

export default ListarTareasAtrasadas;