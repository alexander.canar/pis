import '../css/listaTareas.css'
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { obtenerListaTareas } from '../hooks/Conexion';
import { ListaMaterias } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useNavigate, useParams} from 'react-router-dom';
import DataTable from 'react-data-table-component';
import Modal from 'react-bootstrap/Modal';

const TareasCreadas = () => {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const [data, setData] = useState([]);
  const navegation = useNavigate();
  const [docente, setDocente] = useState('');
  const { external } = useParams()
  useEffect(() => {
    obtenerListaTareas()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSession();
          mensajes(info.mensaje);
          navegation("/Login");
        } else {
          console.log(info);
          setData(info);
          
        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);
  const columns = [
    {
      name: 'Tema',
      selector: (row) => row.tema
    },
    {
      name:'descripcion',
      selector:(row)=> row.descripcion
    },
    {
      name:'fecha_envio',
      selector:(row)=> row.fecha_envio
    },
    {
      name:'fecha_entrega',
      selector:(row)=> row.fecha_entrega
    },
    {
      name: 'Acciones',
      cell: (row) => (
        <button className="btn btn-primary" onClick={() => handleAgregarTarea(row.external_id)}>
         Ver entregas
        </button>
      ),
    },
  ];
  const handleAgregarTarea = (external) => {
    navegation(`/CrearTareas/${external}`);
  };
  return (
    <>
       <div className='col-12' id='usuario'><br />
       <center><h1>LISTA DE TAREAS</h1></center>
       <br />
      <center>
        <div className="input-group">
          <input type="text" className="form-control bg-light border-0 small" placeholder="Buscar" aria-label="Search" aria-describedby="basic-addon2" />
          <button className="btn btn-primary" type="button" style={{ margin: "0px", height: '40px' }}>Bucar</button>
        </div><br />
        <div className='container-fluid'>
          <a className='btn btn-success' role="button" onClick={() => handleAgregarTarea(external)}>Crear Nueva Tarea</a>
        </div>
      </center>
    </div>
  
    <DataTable columns={columns} data={data} expandableRows expandableRowsComponent={ExpandedComponent} />

    <div className="model_box">
      <Modal backdrop="static" keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>Lista</Modal.Title>
        </Modal.Header>
        <Modal.Body>{/* Contenido del modal */}</Modal.Body>
      </Modal>
    </div>
  </>
    
  );
};

export default TareasCreadas;