'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var materia = models.materia;
var estudiante = models.estudiante;
var materia = models.materia;
var profesor = models.profesor;

var tarea = models.tarea;
var cursa = models.cursa;
const bcrypt = require('bcrypt');
const salRounds = 8;
class CursaController {
    async extraerIdCursa(req, res) {
        try {
        var lista = await cursa.findAll({
            attributes: ['external_id'],
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }
    async extraerTodo(req, res) {
        try {
        var lista = await cursa.findAll({
            attributes: ['external_id'],
            include: [
              {
                model: materia,
                as:'materia',
                attributes: ['nombre','unidades'],
              },
              {
                model: profesor,
                as:'profesor',
                attributes: ['nombres'],
              }
            ],
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }
    async extraerNombreTarea(req, res) {
        try {
        var lista = await tarea.findAll({
            attributes: ['tema','descripcion','fecha_envio','fecha_entrega'],
           
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {

            var estudiante_id = req.body.external_estudiante;
            var profesor_id = req.body.external_profesor;
            var materia_id = req.body.external_materia;
                let estuAux = await estudiante.findOne({ where: { external_id:estudiante_id} });
                let profAux = await profesor.findOne({ where: { external_id:profesor_id  } });
                let matAux = await materia.findOne({ where: { external_id: materia_id} });
                console.log(matAux);
                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        id_profesor: profAux.id,
                        id_materia: matAux.id,
                        id_estudiante: estuAux.id,
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await cursa.create(data);
                        await transaction.commit()
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
        
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}
module.exports = CursaController;