'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var profesor = models.profesor;
var rol = models.rol;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const salRounds = 8;
class ProfesorController {
    async listar(req, res) {
        var listar = await profesor.findAll({
            attributes: ['Nombres']
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: listar });

    }
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await profesor.findOne({
            where: { external_id: external }, include: { model: models.cuenta, as: "cuenta", attributes: ['email'] },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'tipo_identificacion', 'edad','telefono']
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var rol_id = req.body.external_rol;
         
            if (1===1) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
                console.log(req.body.email);
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(salRounds), null);
                    };

                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        identificacion: req.body.identificacion,
                        tipo_identificacion: req.body.tipo_identificacion,
                        nombres: req.body.nombres,
                        edad: req.body.edad,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        telefono:req.body.telefono,
                        id_rol: rolAux.id,
                        cuenta: {
                            email: req.body.email,
                            clave: claveHash(req.body.clave)
                        }
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await profesor.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction } );
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    async modificar(req, res) {
        var person = await profesor.finOne({ where: { external_id: req.body.external } });
        if (profesor === null) {
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.apellidos = req.body.apellidos;
            person.nombres = req.body.nombre;
            person.direccion = req.body.direccion;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }
    async guardarPractica(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
               
                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        descripcion: req.body.descripcion,
                        tema: req.body.tema,
                        fecha_envio:req.body.fecha_envio,
                        fecha_entrega:req.body.fecha_entrega
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await tarea.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}
module.exports =ProfesorController;