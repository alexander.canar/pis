'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var tarea = models.tarea;
var cursa = models.cursa;
var materia = models.materia;
const bcrypt = require('bcrypt');
const salRounds = 8;
class TareaController {
    async listar(req, res) {
        var lista = await tarea.findAll({
            attributes: ['tema','descripcion']
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });

    }
   
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            if (1===1) {
                var cursaid = req.body.external_id;
                console.log(cursaid+"RRRRRRRRRRRRRRRRRRRRRRRRRR");
                let matAux = await cursa.findOne({ where: { external_id: cursaid } });
                
                console.log(matAux.id+"TTTTTTTTTTTTTTT");
                if (matAux) {
                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        id_cursa:matAux.id,
                        tema:req.body.tema,
                        descripcion:req.body.descripcion,
                        fecha_envio:new Date(),
                        fecha_entrega:req.body.fecha_entrega,
                        estado:true
                    }
                /* extreaer el id curssa de la materia que esta en el backend para guardarla  a la tarea y mostrarla al estudianet */
                    let transaction = await models.sequelize.transaction();
                    try {
                        await tarea.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    async modificar(req, res) {
        var person = await tarea.finOne({ where: { external_id: req.body.external } });
        if (tarea === null) {
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.apellidos = req.body.apellidos;
            person.nombres = req.body.nombre;
            person.direccion = req.body.direccion;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }
}
module.exports =TareaController;