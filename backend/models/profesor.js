'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const profesor = sequelize.define('profesor', {
    nombres: { type: DataTypes.STRING(50), defaultValue: 'NO_DATA' },
    apellidos: { type: DataTypes.STRING(50), defaultValue: 'NO_DATA' },
    identificacion: {
      type: DataTypes.STRING(20),
      unique: true,
      allowNull: false,
      defaultValue: 'NO_DATA',
    },
    tipo_identificacion: {
      type: DataTypes.ENUM('CEDULA', 'PASAPORTE', 'RUC'),
      defaultValue: 'CEDULA',
    },
    direccion: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    edad: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    estado: { type: DataTypes.BOOLEAN, defaultValue: true },
  }, { freezeTableName: true });

  profesor.associate = function (models) {
    profesor.hasMany(models.cuenta, { foreignKey: 'id_profesor', as: 'cuenta' });
    profesor.belongsTo(models.rol, { foreignKey: 'id_rol'});
  };

  return profesor;
};
