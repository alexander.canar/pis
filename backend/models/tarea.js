'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const tarea = sequelize.define('tarea', {
    id_cursa: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    tema: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    descripcion: {
      type: DataTypes.STRING(300),
      allowNull: false,
    },
    fecha_envio: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    fecha_entrega: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    estado: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    }
  }, {
    freezeTableName: true
  });

  tarea.associate = function(models) {
    tarea.belongsTo(models.cursa, { foreignKey: 'id_cursa', as: 'cursa' });
  };

  return tarea;
};
